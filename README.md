
fs_watch
========
- The application watches a given directory for changes
- Changes involves: add, change, unlink

Philosophy
----------
- To demonstrate usage of express.js, socket.io, angular.js in conjunction
- By-product: file system watching, responsive table, documentation, namespacing (compactible with JSDoc), quick reference / inspiration

**SOME MORE:**
- **socket.io:** basic communication, namespaces
- **express.js:** custom view rendering
- **angular.js:** scope, scope binding, date filter, custom filter, application, controller, routes, templates, factories

Start:
------
- **location:** *localhost:8080/dashboard*
```sh
npm start
```

Test:
-----
Watch console output and dashboard
```sh
sleep 5 && touch fileee &&
sleep 5 && echo 'Hello World!' >> fileee &&
sleep 5 && mv fileee filerrr &&
sleep 5 && rm filerrr
```

Generate Documentation:
-----------------------
```sh
npm run-script generate-docs
```


Scripts & Modules To Mention
----------------------------

### /bin/start
- Initialization script
- Inits *http_server*, starts listening
- Inits eapp
- Inits ioapp
- Listens for server errors

### /http_server.js
- Exports http server
- Dependent by *ioapp* module, *start* script
- *Request* handler is *eapp* and is binded in *start* script
- The server is started in *start* script

### /eapp.js
- Express.js application
- *Request* handler of the http server
- Defines middleware (html rendering / views, static, routes)
- Requires routers

### /ioapp.js
- Socket.io application
- Depends on */http_server.js*
- Requires */io_namespaces/* (after the module is exported)

### /lib/
- Modules - objects (lowerCamelCase), classes (UpperCamelCase)
- Provides some logic or model rather than an interface between a user

### /lib/fsWatch.js
- This module watches for changes in fs, provides data for user
- Used by */routers/dashboard.js*, */io_namespaces/FsWatch.js*

### /io_namespaces/
- Are not exported
- Handles realtime communication within a (io) namespace

### /io_namespaces/FsWatch.js
- **Emits to io:** fsChange, fsError
- **Listens from io:** -

### /routers/
- Express.js routers

### /routers/dashboard.js
- **Routes:** /, /list

### /static/tmpl/
- Client side templates (angular.js)
