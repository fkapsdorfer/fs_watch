
'use strict'

var fs = require('fs');


var configStr = fs.readFileSync('config.json', { flag: 'r' });
var CONFIG = JSON.parse(configStr.toString());

CONFIG.GLOBAL_ROOT = CONFIG.PROTOCOL + '://' + CONFIG.HOST + ':' + CONFIG.PORT;


module.exports = CONFIG;
