/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2015
 * @license MIT
 * @file This document defines {@link Lib_FsWatch} class and exports {@link module:Lib_fsWatch} module.
 */

'use strict'

var CONFIG 		= require('./config');
var fs 			= require('fs');
var chokidar 	= require('chokidar');


/**
 * Watches the {@link module:config.WATCH_DIR} directory.
 * Implicitly starts watching.
 *
 * @class Lib_FsWatch
 * @requires module:config
 * @requires module:fs
 * @requires module:chokidar
 */
function Lib_FsWatch() {
	/**
	 * @memberof Lib_FsWatch
	 * @prop {Object} callbacks
	 * @prop {Lib_FsWatch~errorCb} 	callbacks.error 	error callback
	 * @prop {Lib_FsWatch~allCb} 	callbacks.all 		change callback
	 */
	this.callbacks = {};
	/**
	 * @callback Lib_FsWatch~errorCb
	 * @param 	{String} 	errmsg
	 */
	/**
	 * @callback Lib_FsWatch~allCb
	 * @param 	{Object} 	data 		see {@link Lib_FsWatch~statCb}
	 * @param 	{String} 	data.event 	(add, change, unlink)
	 * @param 	{Bool} 		[data.err] 	if error
	 * @param 	{String} 	[data.msg] 	if error
	 */

	this._init();
}

/**
 * @private
 */
Lib_FsWatch.prototype._init = function() {
	var self = this;
	this.ready = false;

	this._watcher = chokidar.watch(CONFIG.WATCH_DIR, {
		ignored: /[\/\\]\./, 	// hidden files (seem to ingnore ~ backup files automatically)
		persistent: true,
		depth: 0,
		usePolling: false
	});

	this._watcher.on('error', function(e){ self._onError(e) });
	this._watcher.on('all', function(e, p){ self._onAll(e, p) });

	this._watcher.on('ready', function(){
		self.ready = true;
		console.log('(Ready): watching for changes: ', CONFIG.WATCH_DIR);
	});
}


/**
 * Initializes watching (if not already).
 */
Lib_FsWatch.prototype.start = function() {
	// if already is being watched
	if (this._watcher != null) return;

	this._init();
}

/**
 * Stops watching (if not already).
 */
Lib_FsWatch.prototype.stop = function() {

	// has never started watching
	if (this._watcher == null) return;

	this._watcher.close();
	this._watcher = null;
}


/**
 * Called when an error ocured while watching the directory.
 *
 * @private
 * @param {Error} 	err
 */
Lib_FsWatch.prototype._onError = function(err) {
	if (
		this.ready &&
		typeof this.callbacks.error === 'function'
	) {
		this.callbacks.error(err.message);
	}

	console.error('Error while watching fs. ' + err.message);
}

/**
 * Called when a watched file (within the directory) has changed.
 *
 * @private
 * @param {Sting} 	event
 * @param {String} 	path 	a relative path from the local root
 */
Lib_FsWatch.prototype._onAll = function(event, path) {
	if (
		event !== 'change' &&
		event !== 'unlink' &&
		event !== 'add'
	) {
		return;
	}

	if (
		this.ready &&
		typeof this.callbacks.all === 'function'
	) {
		if (event === 'change' || event === 'add') {
			var self = this;

			this.stat(path, function(err, stats){
				if (err) {
					stats.err = true;
					stats.msg = 'An error happened. Cannot analyze a change.';
				} else {
					stats.event = event;
				}
				self.callbacks.all(stats);
			});
		} else { // unlink
			this.callbacks.all({
				event 	: event,
				fname 	: this.pathToFname(path)
			});
		}
	}

	console.error('Lib_FsWatch - event: %s; path: %s', event, path);
}

/**
 * @param 	{String} path 	a relative path from the local root
 * @return 	{String} 		fname
 */
Lib_FsWatch.prototype.pathToFname = function(path) {
	var arr = path.split('/');

	return arr[arr.length -1];
}

/**
 * @param {String} 				path 	a relative path from the local root
 * @param {Lib_FsWatch~statCb} 	cb
 */
Lib_FsWatch.prototype.stat = function(path , cb) {
	var self = this;

	fs.stat(path, function(err, stats){
		if (err) {
			cb(err);
			console.error('Error while fs stat. ' + err.message);
		} else {
			cb(null, {
				fname 	: self.pathToFname(path),
				size 	: stats.size,
				mtime 	: stats.mtime,
				ctime 	: stats.ctime
			});
		}
	});
}
/**
 * @callback Lib_FsWatch~statCb
 * @param 	{Error} 	[err]
 * @param 	{Object} 	stats
 * @param 	{String} 	stats.fname
 * @param 	{Number} 	stats.size
 * @param 	{Date} 		stats.mtime
 * @param 	{Date} 		stats.ctime
 */

/**
 * Lists all files in the watched directory.
 *
 * @param {Lib_FsWatch~listCb} 	cb
 */
Lib_FsWatch.prototype.list = function(cb) {
	var self = this;
	var statsArr = [];
	var len;

	// to determine when to call the callback
	var runningStats 	= 0;
	var allFilesListed 	= false;

	fs.readdir(CONFIG.WATCH_DIR, function(err, fnames){
		len = fnames.length;

		if (!len) {
			cb([]);
		}

		for (var i=0; i<len; i++) {
			var fname = fnames[i];
			var fpath = CONFIG.WATCH_DIR + '/' + fname;

			if (i === len -1) {
				allFilesListed = true;
			}

			// if a hidden or backup file
			if (fname[0] === '.' || fname[fname.length -1] === '~') {
				// if all done
				if (runningStats === 0 && allFilesListed) {
					cb(statsArr);
				}
			} else {
				runningStats++;

				self.stat(fpath, function(err, stats){
					// if an error occured pass null
					statsArr.push(stats || null);

					// if all done
					if (runningStats === 1 && allFilesListed) {
						cb(statsArr);
					}

					runningStats--;
				});
			}
		}
	});
}
/**
 * @callback Lib_FsWatch~listCb
 * @param 	{Array} 	stats
 * @param 	{Object} 	stats[] 	see {@link Lib_FsWatch#stats}
 */


/**
 * Instance of {@link Lib_FsWatch}.
 *
 * @module {Lib_FsWatch} Lib_fsWatch
 */
module.exports = new Lib_FsWatch();
