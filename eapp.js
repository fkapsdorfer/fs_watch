
'use strict'

var fs 		= require('fs');
var express = require('express');
var app 	= express();


app.use(function(req, res, next){
	// overwrite rendering
	res.render = function(viewName) {
		fs.readFile('./views/' + viewName + '.html', {}, function(err, data){
			if (err) {
				res.end();
				console.error('Cannot read a view: ' + viewName);
			} else {
				res.end(data);
			}
		});
	}

	next();
});

app.use('/static', express.static(__dirname + '/static'));
app.use('/libs', express.static('/srv/www/libs.local'));
app.use('/dashboard', require('./routers/dashboard'));


module.exports = app;
