var CONFIG = {
	GLOBAL_ROOT: 'http://' + window.location.host
};


var app = angular.module('fileList', ['ngRoute']);

app.config(function($routeProvider){
	$routeProvider.when('/', {
		controller: 'ListFilesCtrl',
		templateUrl: '/static/tmpl/fileList.html'
	});
});


app.controller('ListFilesCtrl', function($scope, listFilesFactory, socketFactory){

	// load
	listFilesFactory.listFiles()
		.success(function(data){
			$scope.files = data;
		})
		.error(function(){
			console.error('Cannot retreive file list.');
		});

	// listen for changes
	socketFactory.on('FsChange', function(data){
		var file 	= null;
		var index 	= null;
		var e 		= data.event;

		if (data.err) {
			console.error('FsChange: ' + msg);
			return;
		}

		if (e !== 'add') {
			for (var i = 0; i < $scope.files.length; i++) {
				var f = $scope.files[i];

				if (f.fname === data.fname) {
					file 	= $scope.files[i];
					index 	= i;
					break;
				}
			}
		}

		$scope.$apply(function(){
			delete data.event;

			if (e === 'change') {
				$scope.files[index] = data;
			}
			else if (e === 'add') {
				$scope.files.push(data);
			}
			else if (e === 'unlink' && index !== null) {
				$scope.files.splice(index, 1);
			}
		});
	});

	// listen for errors
	socketFactory.on('error', function(msg){
		console.error('FsError: ' + msg);
	});
});


// binary size
app.filter('bsize', function(){
	return function(input){
		var unitList 	= ['B', 'KiB', 'MiB', 'GiB', 'TiB'];
		var multiplier 	= 0;
		var step 		= 1024;
		var cur 		= input;

		if (!input && input !== 0) {
			return null;
		}

		while (step <= cur) {
			cur /= step;
			multiplier++;
		}

		return cur + ' ' + unitList[multiplier];
	}
});

app.factory('listFilesFactory', function($http){
	var factory = {};

	factory.listFiles = function() {
		return $http.get('/dashboard/list');
	}

	return factory;
});

app.factory('socketFactory', function(){
	return new io.connect(CONFIG.GLOBAL_ROOT + '/FsWatch');
});
