
'use strict'

var server 	= require('./lib/http_server');
var app 	= require('socket.io')(server);


module.exports = app;



// ==================== //
// socket io namespaces //
// ==================== //
// note: ioapp needs to be exported


require('./io_namespaces/FsWatch');
