
'use strict'

var ioapp 	= require('../ioapp');
var fsWatch = require('../lib/fsWatch');


var ns = ioapp.of('/FsWatch');

ns.on('connection', function(){
	console.log('/FsWatch - someone connected');
});


fsWatch.callbacks = {
	error: function(msg){
		ns.emit('FsError', msg);
	},
	all: function(data){
		ns.emit('FsChange', data);
	}
}
