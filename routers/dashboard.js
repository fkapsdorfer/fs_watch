
'use strict'

var router = require('express').Router();
var fsWatch = require('../lib/fsWatch');


router.get('/', function(req, res, next){
	res.render('dashboard');
});

router.get('/list', function(req, res, next){
	fsWatch.list(function(list){
		res.end(JSON.stringify(list));
	});
});


module.exports = router;
